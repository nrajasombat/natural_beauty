This is a skeleton of a site I was creating with next js
------
I chose next.js for a few reasons

1. Minimal Configuration. I don't know webpack and I think it is hard. It is like create react apps, all the css and concerns are preconfigured
2. The site I was going create was mainly content, and I wanted server side rendering for SEO
3. It was interoperable with express, and someday I may need to write some api for it
4. It is a simple way of thinking, everything in pages was a different page with its own bundles(code splitting)

Dependancies
----------

I personally suggest nvm, I just started using it

```node v9.4.0```

```npm 5.6.0```

Running in dev
---------

```nvm use```

```npm install```

```npm run dev```

Running in prod
----------
```nvm use```

```npm install```

```npm run build```

```npm run start```

