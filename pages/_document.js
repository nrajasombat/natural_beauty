/**
 * Created by tharajasombat on 2/26/18.
 */
// ./pages/_document.js
import React from 'react';
import Document, { Head, Main, NextScript } from 'next/document';
import Footer from '../components/Footer';
import Navbar from '../components/Navbar';

export default class MyDocument extends Document {
  render() {
    return (
      <html lang="en-US">
        <Head>
          <link
            href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"
            rel="stylesheet"
          />
          <link
            rel="stylesheet"
            href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
            integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u"
            crossOrigin="anonymous"
          />
          <link
            rel="stylesheet"
            href="/_next/static/style.css"
          />
          <meta
            name="viewport"
            content="width=device-width, initial-scale=1, shrink-to-fit=no"
          />
          <style>
                    @import url('https://fonts.googleapis.com/css?family=Lato|Montserrat|Oswald|Raleway');
          </style>

        </Head>

        <body>
          <Main />
          <NextScript />
        </body>
      </html>

    );
  }
}
