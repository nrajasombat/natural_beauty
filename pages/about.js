/**
 * Created by tharajasombat on 2/26/18.
 */
import React from 'react';
import {
  Grid,
  Row,
  Col,
  PageHeader,

} from 'react-bootstrap';
import Footer from '../components/Footer';
import Navbar from '../components/Navbar';
import './index.scss';

export default class About extends React.Component {
  render() {
    return (
      <div>

        <Navbar />
        <Grid>
          <Row className="show-grid">
            <Col lg={12}>
              <PageHeader>
                <h1>Dr. Sook Hong </h1>
              </PageHeader>
            </Col>
          </Row>
          <Row className="show-grid">
            <Col md={6} mdPush={6}>
              <p>
                                Dr. Hong is a biological dentist who believes that the mouth is an integral
                                part of the entire body. Dental problems such as an infection from teeth and gum,
                                cavities from leaking restorations, heavy metal toxicity from mercury fillings and
                                imbalance of bite or airway issues can impact health issues to the body.
                                As a holistic dentist, Dr. Hong is constantly researching and careful in choosing
                                bio-compatible dental materials so that it will not adversely affect patient’s immune
                                system.
                                Dr. Hong received her DDS degree from the University of Pacific, San Francisco in 1992.
                                Dr. Hong completed Naturopathic training at American College of Integrative Medicine and
                                Dentistry in 2012.

                                Dr. Hong has been coaching many general dentists in Orthodontics and TMJ treatment and
                                also helped many
                                general dentists who are very interested in holistic approach. Dr. Hong have found that
                                doing a thorough
                                job from beginning to end, on each individual patient, has enabled her to maintain a
                                high standard of
                                case progress through to completion.
              </p>
              <h3>
                                Credentials
              </h3>
              <li>
                                University of Pacific,School of Dentistry, San Francisco. 1992
                                Graduated from American College of Integrative Medicine and Dentistry, 2012
                                Board Certified Naturopathic Physician, 2012
                                Mastership through Bio-research Institute
                                Completed 2 years of mini-residency in Orthodontics with Dr. Derek Mohony
                                Completed post graduate course on Advanced Orthodontics and TMJ through Mid-America
                                Orthodontic Society
                                Completed Chirodontics course with Dr. Robert Walker; covered Nutrition, Structural,
                                Cranial Techniques, Dental Techniques
                                Completed Institute of Advance TMJ Studies with Dr. Steve Olmos
              </li>
              <h3>
                                Memberships
              </h3>
              <li>
                                Member of International Association of Oral Medicine and Toxicology (IAOMT)
                                Member of International Association of Biological Dentistry and Medicine(IABDM)
                                Member of Holistic Dental Association (HDA)
                                Member of Price Pottenger Nutrition Foundation
                                Member of American Association of Ozone Therapy (AAO)
                                Member of DAMS (Dental Amalgam Mercury Solutions)
                                Member of American Association of Functional Orthodontics
              </li>
            </Col>
            <Col md={6} mdPull={6}>
              <img width={450} height={550} alt="150x150" src="/static/Sook-Hong-DDS.jpg" />
            </Col>
          </Row>
        </Grid>;
        <Footer />
      </div>
    );
  }
}
