/**
 * Created by tharajasombat on 2/26/18.
 */
import React from 'react';
import Navbar from '../components/Navbar';
import HeaderImage from '../components/HeaderImage';
import OfferingSection from '../components/OfferingSection';
import AppointmentSection from '../components/AppointmentSection';
import MeetDoctor from '../components/MeetDoctor';
import Footer from '../components/Footer';
import QuoteCarousel from '../components/QuoteCarousel'


import './index.scss';

export default class Index extends React.Component {
  render() {
    return (
      <div>
        <Navbar />
        <HeaderImage />
        <AppointmentSection />
        <OfferingSection />
        <MeetDoctor />
          <QuoteCarousel/>
        <Footer />

      </div>
    );
  }
}
