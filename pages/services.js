/**
 * Created by tharajasombat on 2/26/18.
 */
import React from 'react';
import {
  Grid,
  Row,
  Col,
  PageHeader,
} from 'react-bootstrap';
import Navbar from '../components/Navbar';

// import './index.scss';
import OfferingSection from '../components/OfferingSection';

export default class Services extends React.Component {
  render() {
    const services = [];
    for (let i = 0; i < 9; i++) {
      services.push({
        width: 350,
        height: 350,
        alt: '150x150',
        src: '/static/Sook-Hong-DDS.jpg',
      });
    }
    const offerings = [
      {
        imageUrl: '/static/facebeauty.png',
        title: 'PRP',
      },
      {
        imageUrl: '/static/model1stockphoto.png',
        title: 'Regunevated Medicine',

      },
    ];
    return (
      <div>
        <Navbar />
        <PageHeader>
          <h1>Services</h1>
        </PageHeader>
        <OfferingSection
          offerings={offerings}
        />
      </div>
    );
  }
}
/**
 * Created by tharajasombat on 2/27/18.
 */
