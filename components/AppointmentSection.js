import React, { Component } from 'react';
import './AppointmentSection.scss';

export default class AppointmentSection extends Component {
  render() {
    return (
      <div className="firstDivider">
        <h2>Make an appointment! Call <a href="tel:(408)733-6413">408-733-6413&nbsp;</a> or
                        schedule online
        </h2>
      </div>
    );
  }
}
