import React, { Component } from 'react';
import Link from 'next/link';


import './HeaderImage.scss';


export default class HeaderImage extends Component {
  render() {
    return (
      <div className="FrontHeader">
        <div className="header-content">
          <span className="header-subhead"> Say Hello to Nature Beauty </span>
          <div className="content-wrapper">
            <h1> Holistic Doctor Focused on Beauty, Comfort & Lifelong Health</h1>
          </div>
          <Link href="/contact/" prefetch>
            <span className="AppointmentBtn">
            Request An Appointment
              </span>
          </Link>
        </div>
      </div>
    );
  }
}
