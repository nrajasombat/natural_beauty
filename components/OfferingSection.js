import React, {Component} from 'react';
import {
    Grid,
    Row,
    Col
} from 'react-bootstrap';
import './OfferingSection.scss';


export default class OfferingSection extends Component {

    static defaultProps = {
        offerings: [
            {
                linkPath: 'path1',
                imageUrl: "/static/ozone.png",
                title: 'Ozone Therapy',
                description: 'Holistic/Biological Dentists respect that the mouth is an integral part of the entire body. ' +
                'The holistic approach is that the chronic diseases such as degenerative diseases, ' +
                'there could be a underlying cause that may have its origin in the mouth.',
            },
            {
                linkPath: 'path2',
                imageUrl: "/static/TMJ.jpg",
                title: 'Ozone Injections',
                description: 'Ozone therapy in dentistry is a minimally invasive form of dentistry that can be used to treat early ' +
                'tooth caries (cavities). Traditionally, dentists treat cavities by drilling the infected area and filling the tooth',
            },
            {
                linkPath: 'path3',
                imageUrl: "/static/stemcell.png",
                title: 'Title',
                description: 'Dentistry is a constantly evolving field. One standard of practice that has been scrutinized for many ' +
                'years is the use of certain metals in dental procedure Nizzle fizzle dolor rizzle shit, consectetuer adipiscing elizzle. Nullam sapien velit, sizzle volutpat, '

            },
        ]
    };

    render() {
        const {
            offerings
        } = this.props;
        return (
            <div className="offeringServices">
                <Grid>
                    <Row className="show-grid">
                        {
                            offerings.map((offering, index) => (
                                <Col xs={12} sm={6} md={4} className="offeringService">
                                    <div
                                        class="offeringImage"
                                        style={{
                                          backgroundImage: `url(${offering.imageUrl})`,
                                        }}
                                    >
                                    </div>
                                    <h2>{offering.title}</h2>
                                    <div className="offeringDescription">
                                        {offering.description}
                                    </div>
                                    <div className="offeringLink">
                                        <a href={offering.linkPath}>
                                            Learn More
                                        </a>
                                    </div>
                                </Col>
                            ))
                        }
                    </Row>
                </Grid>
            </div>
        );
    }
}
