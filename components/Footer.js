import React, {Component} from 'react';
import {
    Row,
    Col,
    Grid
} from 'react-bootstrap';
import './Footer.scss';


export default class Footer extends Component {
    render() {
        return (
            <div className="footer-content">
                <Grid>
                    <Row>
                        <Col xs={6} md={4}>
                            <img width={200} height={200} src="/static/logo.png" alt="logo"/>

                        </Col>
                        <Col xs={6} md={4}>
                            <h1>Address</h1>
                            < div className="fa fa-2x fa-map-marker">
                                <p>825 E Remington Drive</p>
                                    <p>Suite #2</p>
                                    <p>Sunnyvale, California 94087</p>
                            </div>
                        </Col>
                        <Col xs={6} md={4}>
                            <h1>Contact Us</h1>
                            <div className="fa fa-2x fa-phone">
                                408.733.6413
                            </div>
                            <div className="fa fa-2x fa-envelope">
                                sookhongnd@gmail.com
                            </div>

                        </Col>
                    </Row>
                </Grid>
            </div>
        );
    }
}
