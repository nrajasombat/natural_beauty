import React, { Component } from 'react';
import Link from 'next/link';
import {
  Grid,
  Row,
  Col,
  Image,
} from 'react-bootstrap';


import './MeetDoctor.scss';


export default class MeetDoctor extends Component {
  render() {
    return (
      <div className="meetTheDr">
        <Grid>
          <Row className="show-grid">
            <Col lg={6} sm={12}>
              <span className="highlight-title">
                <div className="text-center">
                                Dr. Sook Hong
                </div>
              </span>
              <span className="highlight-text">
                                Dr Hong's team consists of dedicated individuals who are kind,
                            friendly, and caring. His staff is here to ensure a pleasant experience
                            during your visit to our office. Our team includes experienced and energetic
                            people whose goal is to communicate well with our patients and provide the
                            best care possible.
              </span>
              <Row className="btnRow ">
                <div className="text-center">
                  <Link href="/about" prefetch>
                    <span className="meetTheDrbtn"> Meet Dr Sook Hong </span>
                  </Link>
                </div>
              </Row>
            </Col>
            <Col lg={6} sm={12}>
              <Image className="doctor" src="/static/drhong.png" rounded />
            </Col>
          </Row>
        </Grid>
      </div>
    );
  }
}
