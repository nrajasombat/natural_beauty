/**
 * Created by tharajasombat on 2/26/18.
 */
import React, {Component} from 'react';
import {
    Navbar,
    Nav,
    NavItem,
    Row,
    Col,
} from 'react-bootstrap';
import './Navbar.scss';

const paths = [
    {
        title: 'Home',
        link: '/',
    },
    {
        title: 'About Us',
        link: '/about',
    },
    {
        title: 'Services',
        link: '/services',
    },
    {
        title: 'Gallery',
        link: '/gallery',
    },
    {
        title: 'Patient Resources',
        link: '/patient-resources',
    },
    {
        title: 'Contact Us',
        link: '/contact',
    },
];

export default class NavigationBar extends Component {
    render() {
        return (
            <Navbar staticTop className="NavBar">

                <Row>
                    <div className="logo">
                        <img src="/static/logo.png"/>
                    </div>
                </Row>
                <Row>
                    <Navbar.Toggle />
                </Row>
                <Navbar.Collapse>
                    <Row>
                        <Col lg={12}>
                            <Nav>
                                {
                                    paths.map(path => (
                                        <NavItem href={path.link}>
                                            {path.title}
                                        </NavItem>
                                    ))
                                }
                            </Nav>
                        </Col>
                    </Row>
                </Navbar.Collapse>
            </Navbar>
        );
    }
}
