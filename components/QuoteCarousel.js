import React, { Component } from 'react';
import { Carousel } from 'react-bootstrap';
import './QuoteCarousel.scss';

export default class QuoteCarousel extends Component {
  render() {
    return (
      <div className="testimonial">
        <Carousel>
          <Carousel.Item>
            <img src="/static/rosenblatt-green-background-plant-159062.jpeg" alt="testimonial" />

            <Carousel.Caption>
              <h3>Testimonial 1</h3>
              <p>Summary</p>
            </Carousel.Caption>
          </Carousel.Item>
          <Carousel.Item>
            <img src="/static/rosenblatt-green-background-plant-159062.jpeg" alt="testimonial" />
            <Carousel.Caption>
              <h3>Second slide label</h3>
              <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
            </Carousel.Caption>
          </Carousel.Item>
          <Carousel.Item>
            <img src="/static/rosenblatt-green-background-plant-159062.jpeg" alt="testimonial" />
            <Carousel.Caption>
              <h3>Third slide label</h3>
              <p>Praesent commodo cursus magna, vel scelerisque nisl consectetur.</p>
            </Carousel.Caption>
          </Carousel.Item>
        </Carousel>
      </div>
    );
  }
}
