/**
 * Created by tharajasombat on 2/26/18.
 */
// next.config.js
const withSass = require('@zeit/next-sass');
const withCSS = require('@zeit/next-css');

module.exports = withSass(withCSS());
