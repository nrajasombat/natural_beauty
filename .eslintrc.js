module.exports = {
    "extends": "airbnb",
    rules:{
        "react/jsx-filename-extension": 0,
        "no-unused-vars": 1,
        "react/prefer-stateless-function": 1,
        "no-plusplus": 0,
        "max-len": 1,
        "react/no-array-index-key": 1,
    }
};